/**
 * Created by konan on 25-May-17.
 */

this.system = this.system || {};
(function(){
    "use strict";

    var Assets = function(){};

    Assets.getImage = function(name){
        var image = new createjs.Bitmap(queue.getResult(name));
        return image;
    };

    Assets.playBackgroundSound = function(id,times,volume){
        var sound = createjs.Sound.play(id,{loop:times});
        if(volume){
            sound.volume = volume;
        }
    };

    Assets.playSound = function(id){
        createjs.Sound.play(id);
    };

    Assets.stopSound = function(id){
        createjs.Sound.stop(id);
    };

    Assets.muteSound = function(bool){
        createjs.Sound.muted = bool;
    };

    Assets.optimize = function(target,tickOptimize){
        target.mouseEnabled = false;
        if(tickOptimize){
            target.tickEnabled = false;
        }
    };

    system.Assets = Assets;

})();