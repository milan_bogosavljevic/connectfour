/**
 * Created by konan on 24-May-17.
 */

this.system = this.system || {};
(function(){
    "use strict";

    var GameBoard = function(game,numOfRows,numOfColumns,minimumConnectedToWin){
        this.Container_constructor();
        this.init(game,numOfRows,numOfColumns,minimumConnectedToWin);
    };

    var p = createjs.extend(GameBoard,createjs.Container);
    p.game = null;
    p.numOfRows = 0;
    p.numOfColumns = 0;
    p.mainArray = [];
    p.winningArray = [];
    p.countToWin = 0;
    p.minimumConnectedToWin = 0;

    p.init = function (game,numOfRows,numOfColumns,minimumConnectedToWin) {
        this.game = game;
        this.numOfRows = numOfRows;
        this.numOfColumns = numOfColumns;
        this.minimumConnectedToWin = minimumConnectedToWin;
        this.setMainArray();
        this.setGraphic();
    };

    p.setMainArray = function () {
        for(var r = 0; r < this.numOfRows; r++){
            var row = [];
            for(var c = 0; c < this.numOfColumns; c++){
                row.push(0);
            }
            this.mainArray.push(row);
        }
    };

    p.setGraphic = function () {
        var that = this;

        var background = new createjs.Shape(new createjs.Graphics().beginFill("#44484f").drawRoundRect(-15, -15, 1020, 834,6));
        background.cache(-15,-15,1020,834);
        background.alpha = 0.6;
        this.addChild(background);

        var startX = 45;//30
        var fieldBackground = new createjs.Shape(new createjs.Graphics().setStrokeStyle(4).beginStroke("#73d0fc").drawRoundRect (0, 0, 90, 90,6));
        fieldBackground.alpha = 0.6;
        fieldBackground.cache(0,0,90,90);
        system.Assets.optimize(fieldBackground,true);
        fieldBackground.regX = 45;

        var hit = new createjs.Shape(new createjs.Graphics().beginFill("#44484f").drawRect(0,0,100,100));
        hit.regX = hit.regY = 50;

        for(var i = 0; i < this.numOfColumns; i++){
            var txt = i+1;
            var columnNumber = new createjs.Text(txt,"45px Russo One","#73d0fc");//30px
            columnNumber.textAlign = "center";
            columnNumber.textBaseline = "alphabetic";
            columnNumber.x = startX + (i * 150);//100
            columnNumber.y = -60;//40

            if(this.game.deviceIsMobile){
                columnNumber.hitArea = hit;
                columnNumber.on("click",function(event){
                    //that.game.addCoin(event.target.text); // da bi izbegao buplu proveru u igri if game is paused na liniji 91
                    //event.key = event.target.text; // probati ovako
                    var custom = {};
                    custom.key = event.target.text;
                    that.game.keyIsPressed(custom);
                });
            }

            for(var c = 0; c < this.numOfRows; c++){
                var fb = fieldBackground.clone();
                fb.x = columnNumber.x;
                fb.y = (c * 120);//80
                this.addChild(fb);
            }

            this.addChild(columnNumber);
        }
    };

    p.addCoin = function (keyPressed,playerId) {
        var column = keyPressed - 1;
        var row = this.numOfRows - 1;
        for(var i = row; i >= 0; i--){
            if(this.markField(i,column,playerId)){
                this.game.switchPlayers();
                break;
            }
        }
    };

    p.markField = function (row,column,playerId) {
        if(this.mainArray[row][column] === 0){
            this.mainArray[row][column] = playerId;
            this.showAnimation(row,column,playerId);
            this.checkWin(row,column,playerId);
            return true;
        }else{
            return false;
        }
    };

    p.showAnimation = function (row,column,playerId) {  // inserting coins
        var imagename = playerId === 1 ? "sun" : "moon";
        var img = system.Assets.getImage(imagename);
        system.Assets.optimize(img,false);
        img.x = column * 150;//100
        img.y = -120;//80
        var yPos = row * 120;//80
        img.name = String(row) + String(column);
        this.addChild(img);
        createjs.Tween.get(img).to({y:yPos},1000,createjs.Ease.bounceOut);
    };

    p.checkWin = function (row,column,playerId) { 
        var hasWin = false;
        if(!this.checkBottom(row,column,playerId)){
            this.winningArray = [];
            if(!this.checkLeftAndRight(row,column,playerId)) {
                this.winningArray = [];
                if (!this.checkTopleftAndBottomright(row, column, playerId)) {
                    this.winningArray = [];
                    if (!this.checkTopRightAndBottomLeft(row, column, playerId)) {
                        this.winningArray = [];
                    } else {
                        hasWin = true;
                    }
                } else {
                    hasWin = true;
                }
            }
            else{
                hasWin = true;
            }
        }else{
            hasWin = true;
        }

        if(hasWin){
            this.game.onWin(playerId);
            this.showWinningArray();
        }
        this.game.gameIsPaused = hasWin;
    };

    p.showWinningArray = function () {
      for(var i = 0; i < this.winningArray.length; i++){
          var child = this.getChildByName(this.winningArray[i]);
          createjs.Tween.get(child,{loop:true}).to({alpha:0.3},400).to({alpha:1},400);
      }
    };

    p.checkBottom = function (row,column,playerId) {
        if(row > 3){                                   // no neeed to check , it is impossible win
            return false;
        }else{
            var counter = 1;
            var bottomIndex = row + 1;
            var rowToCheck = this.mainArray[bottomIndex];

            this.updateWinningArray(row,column);

            while(this.checkField(rowToCheck,column,playerId)){
                counter++;
                this.updateWinningArray(bottomIndex,column);
                bottomIndex++;
                if(bottomIndex > (this.numOfRows - 1)){
                    break;
                }
                rowToCheck = this.mainArray[bottomIndex];
            }
            return counter >= this.minimumConnectedToWin;
        }
    };

    p.checkLeftAndRight = function (row,column,playerId) {
        var counter = 1;
        var leftIndex = column - 1;
        var rightIndex = column + 1;
        var rowToCheck = this.mainArray[row];

        this.updateWinningArray(row,column);

        while(this.checkField(rowToCheck,leftIndex,playerId)){  // checking left from inserted coin
            counter++;
            this.updateWinningArray(row,leftIndex);
            leftIndex--;
        }

        while(this.checkField(rowToCheck,rightIndex,playerId)){  // checking right from inserted coin
            counter++;
            this.updateWinningArray(row,rightIndex);
            rightIndex++;
        }

        return counter >= this.minimumConnectedToWin;
    };

    p.checkTopleftAndBottomright = function (row,column,playerId) {
        var counter = 1;
        var leftIndex = column - 1;
        var rightIndex = column + 1;
        var topRowIndex = row - 1;
        var bottomRowIndex = row + 1;

        this.updateWinningArray(row,column);

        if(topRowIndex > -1){
            var upRowToCheck = this.mainArray[topRowIndex];
            while(this.checkField(upRowToCheck,leftIndex,playerId)){  // checking up left from inserted coin
                counter++;
                this.updateWinningArray(topRowIndex,leftIndex);
                leftIndex--;
                topRowIndex--;
                if(topRowIndex < 0){
                    break;
                }
                upRowToCheck = this.mainArray[topRowIndex];
            }
        }

        if(bottomRowIndex < this.numOfRows){
            var bottomRowToCheck = this.mainArray[bottomRowIndex];
            while(this.checkField(bottomRowToCheck,rightIndex,playerId)){  // checking bottom right from inserted coin
                counter++;
                this.updateWinningArray(bottomRowIndex,rightIndex);
                rightIndex++;
                bottomRowIndex++;
                if(bottomRowIndex > (this.numOfRows - 1)){
                    break;
                }
                bottomRowToCheck = this.mainArray[bottomRowIndex];
            }
        }

        return counter >= this.minimumConnectedToWin;
    };

    p.checkTopRightAndBottomLeft = function (row,column,playerId) {
        var counter = 1;
        var leftIndex = column - 1;
        var rightIndex = column + 1;
        var topRowIndex = row - 1;
        var bottomRowIndex = row + 1;

        this.updateWinningArray(row,column);

        if(topRowIndex > -1) {
            var upRowToCheck = this.mainArray[topRowIndex];
            while (this.checkField(upRowToCheck, rightIndex, playerId)) {  // checking up rigth from inserted coin
                counter++;
                this.updateWinningArray(topRowIndex,rightIndex);
                rightIndex++;
                topRowIndex--;
                if(topRowIndex < 0){
                    break;
                }
                upRowToCheck = this.mainArray[topRowIndex];
            }
        }

        if(bottomRowIndex < this.numOfRows){
            var bottomRowToCheck = this.mainArray[bottomRowIndex];
            while(this.checkField(bottomRowToCheck,leftIndex,playerId)){  // checking bottom left from inserted coin
                counter++;
                this.updateWinningArray(bottomRowIndex,leftIndex);
                leftIndex--;
                bottomRowIndex++;
                if(bottomRowIndex > (this.numOfRows - 1)){
                    break;
                }
                bottomRowToCheck = this.mainArray[bottomRowIndex];
            }
        }
        return counter >= this.minimumConnectedToWin;
    };

    p.updateWinningArray = function (row,column) {  // this needs for win animation
        var childName = String(row) + String(column);
        this.winningArray.push(childName);
    };

    p.checkField = function (row,index,playerId) {
        if(index < 0 || index > (this.numOfColumns - 1)){
            return false;
        }
        return row[index] === playerId;
    };
    
    p.resetBoard = function () {
        var that = this;
        var numChildren = this.numChildren;

        for(var i = 57; i < numChildren; i++){ // first 57 children are graphics for numbers and background
            var child = this.getChildAt(i);
            that.startRemoveAnimation(child);
        }
        this.mainArray = [];
        this.winningArray = [];
        this.setMainArray();
    };

    p.startRemoveAnimation = function (child) {  // vacuuming animation
        var that = this;
        createjs.Tween.get(child).to({x:459,y:363,alpha:0},1500,createjs.Ease.getPowInOut(2)).call(function () {//306,242
            that.removeChild(child);
        });
    };

    system.GameBoard = createjs.promote(GameBoard,"Container");

})();