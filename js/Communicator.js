/**
 * Created by Conan on 28.2.2017..
 */

this.system = this.system || {};
(function(){
    "use strict";

    var Communicator = function(game){
        this.init(game);
    };

    var _this;
    var p = Communicator.prototype;
    p.connection = null;
    p.game = null;


    p.init = function (game) {
        console.log("Communicator init");
        _this = this;
        this.game = game;
        this.makeConnection();
    };

    p.makeConnection = function () {
        this.connection = io.connect("http://localhost:3000");

        this.connection.on("resetGame",function () {
            console.log("reseting");
            _this.game.startResetAnimation();
        });

        this.connection.on("opponentInfo",function (opponentInfo) {
            if(opponentInfo){
                console.log("calling game");
                _this.game.setOpponentInfo(opponentInfo);
            }
        });

        this.connection.on("showPlayerMove",function (column) {
            console.log("player made move " + _this.game.gameIsPaused);
            _this.game.addCoin(column);
        });

        this.connection.on("returnplayer",function (player) {
            _this.game.setPlayerInfo(player);
        });

        this.connection.on("setPlayerSerial",function (serial) {
            _this.game.setPlayerSerial(serial);
        });

        this.connection.on("opponentDisconnected",function () {
            _this.game.opponentDisconnected();
        });
    };
    
    p.setPlayerInfo = function (id) {
        this.connection.emit("getuser",id);
    };

    p.playerPlayed = function (column) {
        this.connection.emit("playerPlayed",column);
    };

    p.getOpponentInfo = function (index) {
        var opponentIndex = index%2 === 0 ? (index-2) : index; // all players are in connectedPlayers arr on server and all have serial nums
                                                               // first connected player has serial 1 , sescond 2 and so on
        this.connection.emit("getOpponentInfo",opponentIndex); // if player's serial is 3 he needs index 3 in that arr because that is player that connected after him
    };                                                         //  if player's serial is 4 he needs index 2 in that arr because that is player that connected before him

    p.gameIsReseting = function () {
        console.log("game is reseting");
        this.connection.emit("gameIsReseting");
    };
    
    p.updatePlayerScore = function (username,won,lost) {
        console.log("updating score");
        this.connection.emit("updatePlayerScore",username,won,lost);
    };

    system.Communicator = Communicator;

})();