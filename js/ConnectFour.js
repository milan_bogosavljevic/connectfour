/**
 * Created by Milan on 2/23/2017.
 */

this.system = this.system || {};
(function(){
    "use strict";

    var ConnectFour = function(playerId){
        this.Container_constructor();
        this.init(playerId);
    };

    var p = createjs.extend(ConnectFour,createjs.Container);
    p.communicator = null;
    p.player = null;
    p.opponent = null;
    p.activePlayer = null;
    p.gameBoard = null;
    p.gameIsPaused = true;
    p.scoreBoard = null;
    p.infoBackground = null;
    p.resetButton = null;
    p.infoButton = null;
    p.flyingObjects = [];
    p.deviceIsMobile = false;
    p.coinsInserted = 0;

    var _this;

    ConnectFour.GAME_WIDTH = 0;
    ConnectFour.GAME_HEIGHT = 0;
    ConnectFour.NUMBER_OF_ROWS = 0;
    ConnectFour.NUMBER_OF_COLUMNS = 0;
    ConnectFour.MINIMUM_CONNECTED_TO_WIN = 0;

    p.init = function (playerId) {  // player id from mongodb
        _this = this;

        ConnectFour.GAME_WIDTH = 1920;
        ConnectFour.GAME_HEIGHT = 1080;
        ConnectFour.NUMBER_OF_ROWS = 7;
        ConnectFour.NUMBER_OF_COLUMNS = 7;
        ConnectFour.MINIMUM_CONNECTED_TO_WIN = 4;

        this.communicator = new system.Communicator(this);
        this.communicator.setPlayerInfo(playerId);

        if(is.desktop()){                                           // if device is mobile there is no need to set key listeners
            window.addEventListener("keydown",this.keyIsPressed);
        }else{
            this.deviceIsMobile = true;
        }

        this.addComponents();
    };

    p.setPlayerInfo = function (player) {
        this.player = new system.Player(player); // player object from db
    };

    p.setPlayerSerial = function (serial) {  // this is number of connected players , not good solution
        this.player.serialNum = serial%2 === 0 ? 2 : 1; // setting player 1 or player 2 because active player is 1 or 2 no matter what his serial num is
        this.communicator.getOpponentInfo(serial);      // here needs to know exactly what serial is because of connectedPlayers arr on server side
        this.waitForOpponent(serial);
    };

    p.keyIsPressed = function (e) {
        var column = e.key;
        if(!_this.gameIsPaused){
            if(_this.player.serialNum === _this.activePlayer){
                _this.communicator.playerPlayed(column);        // show your move to opponent
                _this.addCoin(column);
            }
        }
    };

    p.addCoin = function (key) {
        if(key <= ConnectFour.NUMBER_OF_COLUMNS && key >= 1){
            this.gameBoard.addCoin(key,this.activePlayer);
        }
    };

    p.setOpponentInfo = function (opponent) {     // opponent info from mongo
        this.opponent = opponent;
    };

    p.waitForOpponent = function (index) {
        var waitTxt = new createjs.Text("Waiting for opponent...","60px Russo One","#efbe00");
        waitTxt.x = 620;
        waitTxt.y = 30;
        this.addChild(waitTxt);
        var interval = setInterval(function () {
            console.log("checking..");
            if(!_this.opponent){
                _this.communicator.getOpponentInfo(index);  // this is player's serial
            }else{
                _this.removeChild(waitTxt);
                _this.setScoreBoard();
                clearInterval(interval);
            }
        },2000);
    };

    p.setScoreBoard = function () {
        var scoreBoard = this.scoreBoard = new system.ScoreBoard(this.player,this.opponent,this.activePlayer);
        this.addChild(scoreBoard);
        this.gameIsPaused = false;
    };

    p.addComponents = function () {
        var background = system.Assets.getImage("background");
        system.Assets.optimize(background,true);
        this.addChild(background);
        this.setFlyingObjects();

        var gameBoard = this.gameBoard = new system.GameBoard(this,ConnectFour.NUMBER_OF_ROWS,ConnectFour.NUMBER_OF_COLUMNS,ConnectFour.MINIMUM_CONNECTED_TO_WIN);
        gameBoard.x = 465;
        gameBoard.y = 240;

        var infoBackground = this.infoBackground = system.Assets.getImage("info");
        system.Assets.optimize(infoBackground,false);
        infoBackground.x = 450;
        infoBackground.y = 225;
        infoBackground.visible = false;

        this.activePlayer = 1;

        var resetButton = this.resetButton = new system.ShapeButton(150,45,6,"#73d0fc");
        resetButton.x = 1824;
        resetButton.y = 1014;
        resetButton.addText("RESTART","30px Russo One","#44484f",0,33);
        resetButton.enableClick(false);

        resetButton.on("click",function(event){
            resetButton.doClickAnimation();
            _this.startReset();
        });

        var infoButton = this.infoButton = new system.ShapeButton(150,45,6,"#73d0fc");
        infoButton.x = 96;
        infoButton.y = 1014;
        infoButton.addText("INFO","30px Russo One","#44484f",0,33);

        infoButton.on("click",function(event){
            infoButton.doClickAnimation();
            _this.showInfo();
        });

        this.addChild(gameBoard,infoBackground,resetButton,infoButton);

        setInterval(function () {
            _this.addFlyingObject();
        },6000);
    };

    p.setFlyingObjects = function () {
        var astronaut = system.Assets.getImage("astronaut");
        system.Assets.optimize(astronaut,false);
        astronaut.visible = false;
        astronaut.traveling = false;
        astronaut.flyType = "horizontal";
        astronaut.canRotate = false;
        astronaut.speed = 20000;
        this.flyingObjects.push(astronaut);

        var astronaut2 = system.Assets.getImage("astronaut2");
        system.Assets.optimize(astronaut2,false);
        astronaut2.visible = false;
        astronaut2.traveling = false;
        astronaut2.flyType = "horizontal";
        astronaut2.canRotate = false;
        astronaut2.speed = 20000;
        this.flyingObjects.push(astronaut2);

        var rocket = system.Assets.getImage("rocket");
        system.Assets.optimize(rocket,false);
        rocket.visible = false;
        rocket.traveling = false;
        rocket.flyType = "vertical";
        rocket.canRotate = false;
        rocket.speed = 10000;
        this.flyingObjects.push(rocket);

        var satelite = system.Assets.getImage("satelite");
        system.Assets.optimize(satelite,false);
        satelite.visible = false;
        satelite.traveling = false;
        satelite.flyType = "horizontal";
        satelite.canRotate = false;
        satelite.speed = 13000;
        this.flyingObjects.push(satelite);

        var shuttle = system.Assets.getImage("shuttle");
        system.Assets.optimize(shuttle,false);
        shuttle.visible = false;
        shuttle.traveling = false;
        shuttle.flyType = "vertical";
        shuttle.canRotate = false;
        shuttle.speed = 13000;
        this.flyingObjects.push(shuttle);

        var ufo = system.Assets.getImage("ufo2");
        system.Assets.optimize(ufo,false);
        ufo.visible = false;
        ufo.regX = ufo.image.width/2;
        ufo.regY = ufo.image.height/2;
        ufo.traveling = false;
        ufo.flyType = "horizontal";
        ufo.canRotate = false;
        ufo.speed = 2000;
        this.flyingObjects.push(ufo);

        var meteor = system.Assets.getImage("meteor");
        system.Assets.optimize(ufo,false);
        meteor.regX = meteor.image.width/2;
        meteor.visible = false;
        meteor.traveling = false;
        meteor.flyType = "diagonal";
        meteor.canRotate = false;
        meteor.speed = 1000;
        this.flyingObjects.push(meteor);

        for(var i = 1; i < 5; i++){
            var name = "asteroid" + i;
            var asteroid = system.Assets.getImage(name);
            system.Assets.optimize(asteroid,false);
            asteroid.visible = false;
            asteroid.regX = asteroid.image.width/2;
            asteroid.regY = asteroid.image.height/2;
            asteroid.traveling = false;
            asteroid.flyType = "horizontal";
            asteroid.speed = 18000;
            asteroid.canRotate = true;
            this.flyingObjects.push(asteroid);
            this.addChild(asteroid);
        }

        this.addChild(astronaut,astronaut2,shuttle,rocket,satelite,ufo,meteor);
    };

    p.addFlyingObject = function () {
        var arrLength = this.flyingObjects.length - 1;
        var index = (Math.round(Math.random() * arrLength));

        var obj = this.flyingObjects[index];
        if(obj.traveling){
            return;
        }

        var props;
        obj.traveling = true;
        obj.visible = true;
        if(obj.flyType === "vertical"){
            obj.x = (Math.round(Math.random() * 900) + 100);
            obj.y = ConnectFour.GAME_HEIGHT;
            props = {y:-100};
        }else if(obj.flyType === "diagonal"){
            obj.y = -90;
            obj.x = this.activePlayer === 1 ? (Math.random()*(ConnectFour.GAME_WIDTH)/2) : ConnectFour.GAME_WIDTH/2 + (Math.random()*(ConnectFour.GAME_WIDTH)/2);
            obj.rotation = this.activePlayer === 1 ? -45 : 45;
            var xToGo = this.activePlayer === 1 ? ConnectFour.GAME_WIDTH : 0;
            props = {y:ConnectFour.GAME_HEIGHT,x:xToGo};
        }else{
            obj.y = (Math.round(Math.random() * 600));
            obj.x = this.activePlayer === 1 ? ConnectFour.GAME_WIDTH+150 : -150;
            props = this.activePlayer === 1 ? {x:-150} : {x:ConnectFour.GAME_WIDTH + 150};
        }

        if(obj.canRotate){
            obj.scaleX = obj.scaleY = Math.random()*2;
            props.rotation = 720;
        }

        createjs.Tween.get(obj).to(props,obj.speed).call(function () {
            obj.visible = false;
            obj.traveling = false;
            if(props.rotation){
                obj.rotation = 0; // must be restarted for next animation
            }
        });
    };

    p.showInfo = function () {
        this.infoBackground.visible = !this.infoBackground.visible;
        this.gameIsPaused = this.infoBackground.visible;
    };

    p.startReset = function () {
        this.startResetAnimation();
        this.communicator.gameIsReseting();
    };

    p.startResetAnimation = function () {

        this.gameIsPaused = true;
        this.resetButton.enableClick(false);
        this.infoButton.enableClick(false);

        var ufo = system.Assets.getImage("ufo");
        ufo.regX = ufo.regY = ufo.image.width/2;
        system.Assets.optimize(ufo,false);
        ufo.x = 2176;
        ufo.y = 644;
        this.addChild(ufo);

        createjs.Tween.get(ufo).to({x:ConnectFour.GAME_WIDTH/2},1000,createjs.Ease.getPowInOut(2)).call(function () {
            _this.gameBoard.resetBoard();
            createjs.Tween.get(ufo).to({rotation:360},2000).to({x:-256},1000,createjs.Ease.getPowInOut(2)).call(function () {
                _this.resetGame();
                _this.removeChild(ufo);
            });
        });
    };

    p.resetGame = function () {
        this.switchPlayers();
        this.gameIsPaused = false;
        this.infoButton.enableClick(true);
    };

    p.switchPlayers = function () {
        this.coinsInserted++;
        if(this.coinsInserted === (ConnectFour.NUMBER_OF_COLUMNS * ConnectFour.NUMBER_OF_ROWS)){
            this.onDraw();
        }else{
            this.activePlayer = this.activePlayer === 1 ? 2 : 1;
            this.scoreBoard.showActivePlayer(this.activePlayer);
        }
    };

    p.onWin = function (playerId) {
        this.coinsInserted = 0;
        this.gameIsPaused = true;
        var winner = this.player.serialNum === playerId ? "player" : "opponent";
        var loser = this.player.serialNum === playerId ? "opponent" : "player";
        this[winner].won++;
        this[loser].lost++;
        this.scoreBoard.updateScore();
        this.scoreBoard.showActivePlayer(0);
        this.communicator.updatePlayerScore(this.player.username,this.player.won,this.player.lost);
        this.switchPlayers();
        this.resetButton.enableClick(true);
    };

    p.onDraw = function () {
        this.coinsInserted = 0;
        this.gameIsPaused = true;
        this.scoreBoard.showActivePlayer(0);
        this.resetButton.enableClick(true);
    };

    p.opponentDisconnected = function () {
        this.scoreBoard.opponentDisconnected();
        setTimeout(function () {
            window.top.location.href = "http://localhost/connectFour";
        },1000);
    };

    p.render = function(e){
        stage.update(e);
    };

    system.ConnectFour = createjs.promote(ConnectFour,"Container");

})();


