/**
 * Created by konan on 24-May-17.
 */

this.system = this.system || {};
(function(){
    "use strict";

    var Player = function(player){
        this.init(player);
    };

    var p = Player.prototype;
    p.username = null;
    p.won = 0;
    p.lost = 0;
    p.serialNum = 0;

    p.init = function (player) {
        this.username = player.username;
        this.won = player.won;
        this.lost = player.lost;
    };

    system.Player = Player;

})();