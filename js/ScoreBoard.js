/**
 * Created by konan on 25-May-17.
 */

this.system = this.system || {};
(function(){
    "use strict";

    var ScoreBoard = function(p1Name,p2Name,activePlayer){
        this.Container_constructor();
        this.init(p1Name,p2Name,activePlayer);
    };

    var p = createjs.extend(ScoreBoard,createjs.Container);

    p.background = null;
    p.activePlayer = null;
    p.player = null;
    p.playerScore = null;
    p.playerSuccess = null;
    p.opponent = null;
    p.opponentScore = null;
    p.opponentSuccess = null;

    p.init = function (player,opponent,activePlayerId) {

        this.player = player;
        this.opponent = opponent;

        var activePlayerName = activePlayerId === this.player.serialNum ? this.player.username : this.opponent.username;
        var txt = activePlayerName.toUpperCase() + "'s turn";
        var activePlayer = this.activePlayer = new createjs.Text(txt,"60px Russo One","#73d0fc");
        system.Assets.optimize(activePlayer,false);
        activePlayer.textAlign = "center";
        activePlayer.textBaseline = "alphabetic";
        var xPos = (system.ConnectFour.GAME_WIDTH/2);
        activePlayer.x = xPos;
        activePlayer.y = 90;
        this.addChild(activePlayer);

        var playerNameTxt = new createjs.Text(this.player.username.toUpperCase(),"45px Russo One","#73d0fc");
        system.Assets.optimize(playerNameTxt,false);
        playerNameTxt.textAlign = "center";
        playerNameTxt.textBaseline = "alphabetic";
        playerNameTxt.x = xPos - 730;
        playerNameTxt.y = 45;

        var playerScore = this.playerScore = new createjs.Text(this.player.won + " WON - LOST " + this.player.lost,"45px Russo One","#efbe00");
        system.Assets.optimize(playerScore,false);
        playerScore.textAlign = "center";
        playerScore.textBaseline = "alphabetic";
        playerScore.x = xPos - 730;
        playerScore.y = 105;

        var percent = this.getWonPercent(this.player.won,this.player.lost);
        var playerSuccess = this.playerSuccess = new createjs.Text("Success: " + percent + "%","45px Russo One","#efbe00");
        system.Assets.optimize(playerSuccess,false);
        playerSuccess.textAlign = "center";
        playerSuccess.textBaseline = "alphabetic";
        playerSuccess.x = xPos - 730;
        playerSuccess.y = 155;
        this.addChild(playerNameTxt,playerScore,playerSuccess);

        var opponentNameTxt = new createjs.Text(this.opponent.username.toUpperCase(),"45px Russo One","#73d0fc");
        system.Assets.optimize(opponentNameTxt,false);
        opponentNameTxt.textAlign = "center";
        opponentNameTxt.textBaseline = "alphabetic";
        opponentNameTxt.x = xPos + 730;
        opponentNameTxt.y = 45;

        var opponentScore = this.opponentScore = new createjs.Text(this.opponent.won + " WON - LOST " + this.opponent.lost,"45px Russo One","#efbe00");
        system.Assets.optimize(opponentScore,false);
        opponentScore.textAlign = "center";
        opponentScore.textBaseline = "alphabetic";
        opponentScore.x = xPos + 730;
        opponentScore.y = 105;

        percent = this.getWonPercent(this.opponent.won,this.opponent.lost);
        var opponentSuccess = this.opponentSuccess = new createjs.Text("Success: " + percent + "%","45px Russo One","#efbe00");
        system.Assets.optimize(opponentSuccess,false);
        opponentSuccess.textAlign = "center";
        opponentSuccess.textBaseline = "alphabetic";
        opponentSuccess.x = xPos + 730;
        opponentSuccess.y = 155;
        this.addChild(opponentNameTxt,opponentScore,opponentSuccess);
    };

    p.showActivePlayer = function (activePlayerId) {
        var that = this;
        var activePlayerName = activePlayerId === this.player.serialNum ? this.player.username : this.opponent.username;
        var txt = activePlayerId > 0 ? activePlayerName.toUpperCase() + "'s turn" : "GAME OVER";
        createjs.Tween.get(this.activePlayer).to({alpha:0},300).call(function () {
            that.activePlayer.text = txt;
            createjs.Tween.get(that.activePlayer).to({alpha:1},300);
        });
    };

    p.updateScore = function () {
        this.playerScore.text = this.player.won + " WON - LOST " + this.player.lost;
        this.playerSuccess.text = "Success: " + this.getWonPercent(this.player.won,this.player.lost) + "%";
        this.opponentScore.text = this.opponent.won + " WON - LOST " + this.opponent.lost;
        this.opponentSuccess.text = "Success: " + this.getWonPercent(this.opponent.won,this.opponent.lost) + "%";
    };

    p.opponentDisconnected = function () {
        this.activePlayer.text = "Opponent has disconnected";
    };

    p.getWonPercent = function (won,lost) {
        var totalPlayed = won + lost;
        var onePercent = totalPlayed/100;
        return Math.round(won/onePercent) || 0;
    };

    system.ScoreBoard = createjs.promote(ScoreBoard,"Container");

})();