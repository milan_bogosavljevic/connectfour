/**
 * Created by Milan on 2/23/2017.
 */

var express = require('express');
var app = express();
var server = app.listen(3000);
var mongoClient = require("mongodb");
var ObjectId = mongoClient.ObjectID; // for finding users by mongo id
var bodyParser = require("body-parser"); // for post requests
var database;
var connectedPlayersArr = [];
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

app.use(allowCrossDomain);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static("public"));

app.post('/login', function(req,res){
    loginUser(req.body,res);
});

app.post('/create', function(req,res){
    createUser(req.body,res);
});

var socket = require('socket.io');
var io = socket(server);
io.sockets.on("connection",newConnection);

mongoClient.connect("mongodb://localhost:27017/connectfour",function (err,db) {  // connectfour = database name
    if(err === null){
        console.log("Connected to database");
        database = db;
    }else{
        console.log("Connection Error: " + err);
    }
});

function loginUser(data,res){
    database.collection("players").findOne({username:data.username,password:data.password},function (err,result) {  // players = table name
        if(result === null){
            res.send({"err":"Wrong username or password"});
        }else{
            var id = result._id;
            if(result.connected === false){
                res.send({"userid":id});  // mongodb id
                database.collection("players").updateOne({_id:ObjectId(id)},{$set:{"connected":true}});
            }else{
                res.send({"err":"User already connected"});
            }
        }
    });
}

function createUser(data,res){
    database.collection("players").find({username:data.username}).count(function (err,count) {
        if(count === 0){
            database.collection("players").insertOne({username:data.username,password:data.password,connected:false,won:0,lost:0},function (err,result) { //add more properties if need
                if(err === null){
                    res.send({"response":"Player Inserted"});
                }else{
                    console.log("Insertion Error: " + err);
                }
            });
        }else{
            res.send({"response":"Username already exist"});
        }
    });
}

function newConnection(socket){

    socket.on("getuser",getUser);
    socket.on("disconnect",disconnectUser);
    socket.on("playerPlayed",playerPlayed);
    socket.on("getOpponentInfo",getOpponentInfo);
    socket.on("gameIsReseting",resetGame);
    socket.on("updatePlayerScore",updatePlayerScore);

    function getUser(id) {
        database.collection("players").findOne({_id:ObjectId(id)},function (err,result) {  // players = table name
            if(result === null){
                console.log("Error");
            }else{
                result.socketId = socket.id;
                socket.emit("returnplayer",result);
                connectedPlayersArr.push(result);
                socket.mongoId = id;     // mongdb id - this needs for disconnect user to know whitch one to set connected property to false
                database.collection("players").find({connected:true}).count(function (err,count) { // just for 2 players for now
                    socket.emit("setPlayerSerial",count);
                });
            }
        });
    }

    function disconnectUser(){
        database.collection("players").updateOne({_id:ObjectId(socket.mongoId)},{$set:{"connected":false}});  // mongodb id
        for(var i = 0; i < connectedPlayersArr.length; i++){
            if(connectedPlayersArr[i]._id == socket.mongoId){
                connectedPlayersArr.splice(i,1);
                socket.broadcast.to(socket.opponentSocketId).emit('opponentDisconnected');
            }
        }
        console.log("disconnect "+connectedPlayersArr.length);
    }

    function playerPlayed(column){
        socket.broadcast.to(socket.opponentSocketId).emit("showPlayerMove",column);
    }

    function getOpponentInfo(index){                                    // if player came first he sends index 1 and gets second player in arr
        if(connectedPlayersArr[index]){                                 // othewise he sends index 0 and gets first player in arr
            socket.emit("opponentInfo" , connectedPlayersArr[index]);        // sending opponent player object
            socket.opponentSocketId = connectedPlayersArr[index].socketId;   // seting to know with whom to communicate
        }
    }

    function resetGame() {
        socket.broadcast.to(socket.opponentSocketId).emit("resetGame");
    }

    function updatePlayerScore(username,won,lost) {
        database.collection("players").updateOne({username:username},{$set:{"won":won,"lost":lost}});
    }

}
